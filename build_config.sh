#!/bin/bash
set -e
apt update 
apt-get install -y g++ gdb build-essential cmake lcov   
apt-get install -y git curl unzip doxygen graphviz nano 
apt-get install -y cppcheck 

