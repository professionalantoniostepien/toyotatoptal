# Change Log

All notable changes to this project will be documented in this file.

## 2020-12-06

Here write all final notes. 

### Added

- Create Dockerfile.build for supporting of build, run test process;

- A Dockerfile.eclipse keep the IDE support for a further improvements;

- Doxygen configuration file to document the source code;

- Scripts shells added to support the automation of this build;

- Added the final instruction for build process using docker or a Host Linux platform;

- Formatted the code style passed on K&R style using eclipse feature;

- Uploaded files into my bitbucket account and added pipeline configuration to run the containers for this project;

  

### Changed

### Fixed

- Minor changes applied on the Buggy.cxx and Buggy.hxx to remove unnecessary comments or functions no longer necessary for this challenge;


## 2020-11-18

### Added

* CMake support including  GoogleTest Framework;

* Test cases include :

  * buggy-test.cpp -> 

    **TestCase, InputMessagesFromTextFile**

    * printTest.txt - List of inputs to be tested
    * resultsTest.txt - Result expected against the inputs defined in the prinTest.txt file

    **TesCase, AddListFromString**

    * Adds a list of entries to test all conditions criteria for:
      * Add 2 elements;
      * Add once of the same previous elements;
      * Add 3 times a same previous element;
      * *Add once a second element*;
      *  Add an element without the the word terminate 'end'. It should not be considered

* Implemented my own modification based on:

  * improve portability and readability 
  * Replaced the struct approach by std::map. As **std**::**map** is a sorted associative container that contains key-value pairs with unique keys. it helps to build a better diction/map of key words and counter values;
  * the main program implemented using the thread based on the Buggy's method in the toyota.cpp;

  

  

### Changed

- After ran this project and realized the structure object used does not support the right approach.

- Method should encapsulated into  class. Changed the project to use buggy.cxx and buggy.hxx as a module;
- Created main function to support the challenge aspects;

## 2020-11-19

### Added

* Added simple make file to compile this project using G++ under C++14 with PTHREAD support;

### Changed

* Modifications to run the initial project;
* Removed and commented lines to make the buggy.cxx works for compilation sake;
* Understood the implementation of this project across the description of PDF document related with challenge;

### Fixed

* gets' is not a member of 'std' and in c++14 is deprecated - Removed