#!/bin/bash

BRANCH_OR_TAG=master
CMAKE_OPTIONS=

git clone --depth=1 -b $BRANCH_OR_TAG -q https://github.com/google/googletest.git /googletest
mkdir -p /googletest/build
cd /googletest/build
cmake .. ${CMAKE_OPTIONS} && make && make install
rm -rf /googletest
