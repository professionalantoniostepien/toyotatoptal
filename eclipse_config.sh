#!/bin/bash
set -e
#install all dependences for supporting the eclipse X11 and build any C++ project using CDT
apt update 
apt-get install -y libx11-6 libfreetype6 x11-apps  g++ gdb build-essential cmake lcov   
apt-get install -y libxrender1 libfontconfig1 libxext6 xvfb curl unzip doxygen graphviz nano 
apt-get install -y git zip wget libnss3 libxcomposite-dev libxi6 libgconf-2-4 libxtst6 
apt-get install -y libpci3  xcb qt5-default  qttools5-dev openjdk-11-jdk cppcheck cppcheck-gui 
