#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <fstream>

#include "buggy.hxx"


using testing::ElementsAre;
using testing::Pair;

std::string loadFile(const std::string& path) {
    auto ss = std::ostringstream{};
    std::ifstream file(path);
    ss << file.rdbuf();
    return ss.str();
}

TEST(TestCase, InputMessagesFromTextFile) {
    Buggy buggy;
    std::string name ;
    std::ifstream dataFile("../test/printTest.txt");
    
    while ( !dataFile.eof() )
    {
          std::getline (dataFile,name);
          buggy.addToList(name);
    }
    
  
    testing::internal::CaptureStdout();
    buggy.printList();

    std::string output = testing::internal::GetCapturedStdout();
    std::string str = loadFile("../test/resultsTest.txt"); 
    ASSERT_EQ(output,str);
}


TEST(TesCase, AddListFromString) {
    Buggy buggy;
    //Add 2 elements
    buggy.addToList("antonioendstepienend");
    EXPECT_THAT(buggy.mWord, ElementsAre(Pair("antonio", 1), Pair("stepien", 1)));
    //Add once of the same previous elements
    buggy.addToList("antonioendstepienend");
    EXPECT_THAT(buggy.mWord, ElementsAre(Pair("antonio", 2), Pair("stepien", 2)));
    //Add 3 times a same previous element
    buggy.addToList("antonioendantonioendantonioend");
    EXPECT_THAT(buggy.mWord, ElementsAre(Pair("antonio", 5), Pair("stepien", 2)));
    //Add once a second element 
    buggy.addToList("stepienend");
    EXPECT_THAT(buggy.mWord, ElementsAre(Pair("antonio", 5), Pair("stepien", 3)));
    //Add an elemenet witout the the word terminate 'end'. It should not be considered
    buggy.addToList("stepien");
    EXPECT_THAT(buggy.mWord, ElementsAre(Pair("antonio", 5), Pair("stepien", 3)));
}

