# Build and Run

Pre-requisites:

* Ubuntu 20.04
* g++ 
* gdb 
* build-essential
* cmake
* GoogleTest

## Build and Run with Docker 

* Pre-requisites
 - Docker Client
 - [Play with Docker] (https://labs.play-with-docker.com/) - Optional



```shell
$ docker-compose up build
```





![](images/docker_compose_build.gif)



```shell
$ docker-compose run build bash ./build.sh
```

![](images/docker_compose_run.gif)

# Build and Run Linux

## Building with CMake


```
$ mkdir build
$ cd build
$ cmake ..
$ make
```
# Run Tests

```
$ test/ToyotaProject_tst
```

# Run Tests

```
$ src/ToyotaProject_run
```

# Folder Structure

```
.
|-- CMakeLists.txt
|-- Dockerfile.build
|-- Dockerfile.eclipse
|-- Doxyfile
|-- build.sh
|-- build_config.sh
|-- docker-compose.yml
|-- eclipse_config.sh
|-- gtest_config.sh
|-- images
|   |-- docker_compose_build.gif
|   |-- docker_compose_run.gif
|   `-- logo.jpg
|-- readme.md
|-- src
|   |-- CHANGELOG
|   |-- CMakeLists.txt
|   |-- buggy.cxx
|   |-- buggy.hxx
|   |-- cppcheck.cppcheck
|   |-- main_page.md
|   |-- toyota.cpp
|   `-- toyota.doxyfile
|-- test
|   |-- CMakeLists.txt
|   |-- buggy-test.cpp
|   |-- main.cpp
|   |-- printTest.txt
|   `-- resultsTest.txt
`-- tree.txt

3 directories, 27 files

```
