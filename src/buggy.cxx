#include "buggy.hxx"
#include <list>
#include <algorithm>

#define DEBUG   1

void Buggy::addToList(const std::string &str) {

	split(str, "end");

}

void Buggy::lookupWords(
		std::pair<std::map<std::string, int>::iterator, bool> &result) {
	// Check if Insertion was successful
	if (result.second == false) {
		// Insertion Failed
		std::map<std::string, int>::iterator it = mWord.find(
				result.first->first);
		if (it != mWord.end()) {
			(*it).second++; // incremented because of the  element is already exist
		}
#if DEBUG
		std::cout << result.first->first
				<< " was NOT found in the initial word list" << std::endl;
#endif
	}
#if DEBUG
	else {
		// Insertion was successful
		std::cout << "SUCCESS: " << result.first->first << " was present "
				<< result.second << " times in the initial word list"
				<< std::endl;
	}
#endif
}

void Buggy::split(std::string stringToBeSplitted, std::string delimeter) {
	std::vector<std::string> splittedString;
	size_t startIndex = 0;
	size_t endIndex = 0;
	std::pair<std::map<std::string, int>::iterator, bool> result;

	while ((endIndex = stringToBeSplitted.find(delimeter, startIndex))
			< stringToBeSplitted.size()) {
		std::string val = stringToBeSplitted.substr(startIndex,
				endIndex - startIndex);

		result = mWord.insert(std::pair<std::string, int>(val, 1));

		lookupWords(result);

		startIndex = endIndex + delimeter.size();
	}

}

// Function to sort the map according
// to value in a (key-value) pairs
void mySort(std::map<std::string, int> &M) {

	// Declare vector of pairs
	std::vector<std::pair<std::string, int> > A;

	// Copy key-value pair from Map
	// to vector of pairs
	for (auto &it : M) {
		A.push_back(it);
	}

	// Sort using comparator function
	std::sort(A.begin(), A.end());

}

void Buggy::printList(void) {

	mySort(mWord);
	//std::map<std::string, int>::iterator it = mWord.begin();

	// Iterate over a map using std::for_each and Lambda function
	std::for_each(mWord.begin(), mWord.end(),
			[](std::pair<std::string, int> element) {
				// Accessing KEY from element
				std::string word = element.first;
				// Accessing VALUE from element.
				int count = element.second;
				std::cout << word << " " << count << std::endl;
			});

	std::cout << "Total words found: " << mWord.size() << std::endl;

}

