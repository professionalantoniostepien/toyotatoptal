#include "buggy.hxx"
#include <thread>
#include <mutex>
#include <condition_variable>

std::mutex g_mutex;
std::condition_variable g_condVar;
bool processed = false;

Buggy buggy;

void workerInputThread() {
	std::string str;
	// Acquire the lock
	std::unique_lock<std::mutex> mlock(g_mutex);

	while (!std::cin.eof()) { // EOF?
		std::cout << "Word list:" << std::endl;
		std::getline(std::cin, str);
		buggy.addToList(str);

	}
	processed = true;
	mlock.unlock();
	g_condVar.notify_one();
}

void workerPrintThread() {
	std::unique_lock<std::mutex> mlock(g_mutex);
	g_condVar.wait(mlock, [] {return processed;});

	buggy.printList();
}

int main() {

	std::cout << "-------------------Started-----------------------"
			<< std::endl;

	std::thread t1(workerInputThread);
	std::thread t2(workerPrintThread);

	t1.join();
	t2.join();

	std::cout << "Done!!" << std::endl;

	return 0;
}

