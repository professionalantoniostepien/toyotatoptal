#include <string>
#include <vector>
#include <sstream>
#include <iostream>
#include <map>

/// @defgroup BuggyClass Buggy
/// @brief Class for Toyota Challenge
/// @{

class Buggy {
public:
	std::map<std::string, int> mWord;

	void lookupWords(
			std::pair<std::map<std::string, int>::iterator, bool> &result);

	/// @brief printList Implementation

	void printList(void);

	///  @brief addToList implementation get the line of string and  add to list of string list
	/// , but rejecting the duplicates  item.
	/// @param str - line os ASCII strings to be splitted and add if follow the delimiter rule

	void addToList(const std::string &str);
private:
	/// @brief split implementation splits ASCII words by using a string delimiter
	/// @param stringToSplit - String input to split
	/// @param delimiter - a String to be used as delimiter
	/// @return
	void split(std::string stringToSplit, std::string delimiter);

};

/// }@

